require('dotenv/config')
const express = require("express");
const multer = require('multer')
const AWS = require('aws-sdk')
AWS.config = new AWS.Config();
 AWS.config.accessKeyId = "AKIA2I6I4XC65BWTEKOU";
 AWS.config.secretAccessKey = "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";
 AWS.config.region = "ap-southeast-1";
const uuid = require('uuid/v4')

const app = express();
const http = require('http');
// const fs = require("fs");
const bodyParser = require("body-parser");
const port = process.env.PORT || 2542;
const server = http.createServer(app);
// const { Server } = require("socket.io");
// const io = new Server(server);
const cors = require('cors')
// const jwt = require("express-jwt");

let whitelist=[
    'https://consent.ach.co.th',
    'http://10.0.30.95',
    'http://10.0.30.25:3001',
    'http://10.0.30.18',
    'http://localhost:3001',
    'http://10.1.1.95:10101',
    'http://10.0.99.228:3000',
    'http://localhost:3000',
    'https://consent.hondamaliwan.com/',
    'https://consent.autoclikfastfit.com/'
    ]
//   let corsOptions = {
//     origin: function (origin, callback) {
//       if (whitelist.indexOf(origin) !== -1) {
//         callback(null, true);
//       } else {
//         callback("Access Denied", false);
//       }
//     },
//     credentials: true
//   }
// app.use(cors(corsOptions))

app.use(cors());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(
  bodyParser.json({
    limit: "20mb",
  })
)
// io.on('connection', (socket) => {
//     socket.on("pending", (msg) => {
//       console.log(msg);
//       socket.broadcast.emit("pending", true);
//     })
//     socket.on("disconect", function ()  {
//       console.log(msg);
//       socket.broadcast.emit("disconect", "false");
//       socket.removeAllListeners();
//     })
//   })




  // API ROUTES

  const vendor = require("./api/vendor/routes");
  app.use("/vendor",vendor)

  // S3 Upload
  
  const s3 = new AWS.S3({
    accesKeyId: process.env.AWS_ID,
    secretAccessKey: process.env.AWS_SECRET,
  
  })
  
  const storage = multer.memoryStorage({
    destination: function(req, file,callback) {
      callback(null, '')
    }
  })
  
  const upload = multer({storage}).single('file')

  app.post('/upload',upload, async (req, res) => {
    
    let myFile = req.file.originalname.split(".")
    const fileType = myFile[myFile.length - 1]

    // console.log(req.file)
    // res.send({
    //   message: "Hello World!"
    // })

    const params = {
      Bucket: process.env.AWS_BUCKET_NAME,
      Key: `${uuid()}.${fileType}`,
      Body: req.file.buffer
    }

    s3.upload(params, async (error, data) => {
      if(error) {
        res.status(500).send(error)
      } 

      res.status(200).send(data)
    })
  })

  server.listen(port, () => {
    console.log('listening on '+port);
  })
