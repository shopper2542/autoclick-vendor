const express = require("express");
const router = express.Router();

const { addVendorInfo, getInfo } = require("../function/vendorInfo")

router
  .route("/info/add")
  .post(async (req, res) => {
    try {
      let response = await addVendorInfo(
        req.body.vendor_company_name,
        req.body.vendor_address,
        req.body.vendor_name,
        req.body.vendor_company_tel,
        req.body.vendor_tel,
        req.body.vendor_email,
        req.body.vendor_line_id,
        req.body.vendor_ss_num,
        req.body.vendor_business_type,
        req.body.vendor_business_age,
        req.body.vendor_fund_amount,
        req.body.vendor_pdf_url

      )

      res.status(200).json(response)
    } catch (error) {
      res.status(500).json({ 'error': error })
    }
  })

  router
  .route("/getVendorInfo")
  .get(async (req, res) => {
    try {
      let response = await getInfo()
      res.status(200).json(response)
    } catch (error) {
      console.log(error);
      res.status(500).json({ 'error': error })
    }
  })

  module.exports = router;  