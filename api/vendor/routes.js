const express = require("express")
const controllers = require("./controllers")
const router = express.Router();

router.use(controllers.vendorInfo)

module.exports = router