const db = require('../../../knex/knex');
require("dotenv").config();
const table_vendor_info = process.env.table_vendor_info

const addVendorInfo = async(vendor_company_name, vendor_address, vendor_name, vendor_company_tel, vendor_tel, vendor_email, vendor_line_id, vendor_ss_num, vendor_business_type, vendor_business_age, vendor_fund_amount, vendor_pdf_url) => {
    let response = db('vendor_info').insert({
        vendor_company_name:vendor_company_name,
        vendor_address:vendor_address,
        vendor_name:vendor_name,
        vendor_company_tel:vendor_company_tel,
        vendor_tel:vendor_tel,
        vendor_email:vendor_email,
        vendor_line_id:vendor_line_id,
        vendor_ss_num:vendor_ss_num,
        vendor_business_type:vendor_business_type,
        vendor_business_age:vendor_business_age,
        vendor_fund_amount:vendor_fund_amount,
        vendor_pdf_url:vendor_pdf_url



    })
    return response


}

const getInfo = () => {
    return db('vendor_info').select(
        'vendor_company_name',
        'vendor_address',
        'vendor_name',
        'vendor_company_tel',
        'vendor_tel',
        'vendor_email',
        'vendor_line_id',
        'vendor_ss_num',
        'vendor_business_type',
        'vendor_business_age',
        'vendor_fund_amount',
        'vendor_pdf_url'
    )
  }


module.exports = { addVendorInfo, getInfo }