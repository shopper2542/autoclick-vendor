FROM node:14.5.0 AS builder
RUN mkdir -p /app
WORKDIR /app
COPY package.json .
RUN npm install --only=prod
COPY . .
EXPOSE 4666
CMD ["node","server.js"]
