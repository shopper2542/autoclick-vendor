const jwt = require("jsonwebtoken");
const fs = require("fs");
global.privateKeyPath = __dirname + "/PrivateKey.key";
const privateKey = fs.readFileSync(privateKeyPath, "utf8");


const verifyToken = (req,res,next)=>{
    try{
        const bearerHeader = req.headers.authorization.split(' ')[1]
       var decoded = jwt.verify(bearerHeader, privateKey);
       next()
    }
    catch(err){
        res.send("Authorization Fail")
    }

       
}

module.exports={verifyToken}